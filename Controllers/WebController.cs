﻿using interfazRobot.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fernet;
using System.IO;
using System.Timers;
using System.Globalization;
using System.Diagnostics;
using Hanssens.Net;
using Hanssens.Net.Helpers;
using System;
namespace interfazRobot.Controllers
{
    public class WebController : Controller
    {
        // GET: WebController
        public static bool ext = true;
        public static string hora;
        public static string minuto;
        public static string segundo;
        private static Timer aTimer;
        private static void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            //Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
            hora = DateTime.Now.ToString("hh");
            minuto = DateTime.Now.ToString("mm");
            segundo = DateTime.Now.ToString("ss");
            //Console.WriteLine(segundo);
        }
        public void temporizadorSegundoPlano(){
            // Create a timer and set a two second interval.
            aTimer = new System.Timers.Timer(2000);
            // Hook up the Elapsed event for the timer.
            aTimer.Elapsed += OnTimedEvent;
            // Have the timer fire repeated events (true is the default)
            aTimer.AutoReset = true;
            // Start the timer
            aTimer.Enabled = true;
        }
        public IActionResult Index()
        {
            temporizadorSegundoPlano();
            return View();
        }
        public IActionResult SetEstadoExternas(){
            // Console.WriteLine("=========funciona el setAction=======");
            ext = !ext;
            return RedirectToAction("formRobot1");
        }
        public IActionResult formRobot1()
        {
            Console.WriteLine("===============================>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            ViewBag.Externas = ext;
            return View();
        }
        [BindProperty]
        public RobotForm1 robotForm1 { get; set; }
        public IActionResult SetForm1(){
            Console.WriteLine("===========Data formulario 1=======");
            //var resform1 = Json(robotForm1);
            Console.WriteLine(robotForm1.userNetbank);
            Console.WriteLine(robotForm1.passwordNetbank);
            Console.WriteLine(robotForm1.userMediador);
            Console.WriteLine(robotForm1.passwordMediador);
            string fechaFile = DateTime.Now.ToString("yyyy-MM-dd");
            string horaFile = DateTime.Now.ToString("HH") + "H" +DateTime.Now.ToString("mm") + "m" + DateTime.Now.ToString("ss") + "s";
            string pathFile = @"D:\adweb\INPUT\Robot1\" + horaFile +"_"+ fechaFile + "-R1.ini";
            using (StreamWriter sw = new StreamWriter(pathFile))
            {
                sw.WriteLine("[NETBANK]");
                sw.WriteLine("user_netbank = " + robotForm1.userNetbank);
                sw.WriteLine("pass_netbank = " + robotForm1.passwordNetbank);
                sw.WriteLine("[MEDIADOR]");
                sw.WriteLine("user_mediador = " + robotForm1.userMediador);
                sw.WriteLine("pass_mediador = " + robotForm1.passwordMediador);
                sw.Close();
            }
            string sw2;
            var key = SimpleFernet.GenerateKey().UrlSafe64Decode();
            string llaveFinal = Convert.ToBase64String(key);
            using (StreamReader sw = new StreamReader(pathFile))
            {
                sw2  = sw.ReadToEnd();
                var src64 = sw2.ToBase64String();
                var token = SimpleFernet.Encrypt(key, src64.UrlSafe64Decode());
                sw.Close();
                using (StreamWriter sw3 = new StreamWriter(pathFile))
                {
                    sw3.WriteLine(token);
                    sw3.WriteLine(llaveFinal);
                    sw3.Close();
                }
            }
            FileInfo file = new FileInfo(pathFile);
            bool flag = file.Exists;
            if(flag){
                return RedirectToAction("Index");
            }else{
                return Content("Archivo 1 no puede ser encontrado!!!");
            }
            // var resForm = Json(Usuario);
        }
        [BindProperty]
        public RobotForm1externas robotForm1externas { get; set; }
        public IActionResult SetForm1Externas(){
            Console.WriteLine("===========Data formulario 1 externas=======");
            //var resform1 = Json(robotForm1);
            Console.WriteLine(robotForm1externas.userNetbank);
            Console.WriteLine(robotForm1externas.passwordNetbank);
            Console.WriteLine(robotForm1externas.userMediador);
            Console.WriteLine(robotForm1externas.passwordMediador);
            Console.WriteLine(robotForm1externas.codEncargadoPlataforma);
            Console.WriteLine(robotForm1externas.codCajeros);
            string fechaFile = DateTime.Now.ToString("yyyy-MM-dd");
            string horaFile = DateTime.Now.ToString("HH") + "H" +DateTime.Now.ToString("mm") + "m" + DateTime.Now.ToString("ss") + "s";
            string pathFile = @"D:\adweb\INPUT\Robot1\" + horaFile +"_"+ fechaFile + "-R1.ini";
            using (StreamWriter sw = new StreamWriter(pathFile))
            {
                sw.WriteLine("[NETBANK]");
                sw.WriteLine("user_netbank = " + robotForm1externas.userNetbank);
                sw.WriteLine("pass_netbank = " + robotForm1externas.passwordNetbank);
                sw.WriteLine("[MEDIADOR]");
                sw.WriteLine("user_mediador = " + robotForm1externas.userMediador);
                sw.WriteLine("pass_mediador = " + robotForm1externas.passwordMediador);
                sw.WriteLine("[ENCARGADOEXT]");
                sw.WriteLine("codigoEnc = " + robotForm1externas.codEncargadoPlataforma);
                sw.WriteLine("[CAJAS]");
                string cajasExt = robotForm1externas.codCajeros;
                cajasExt = "["+cajasExt.Substring(0,cajasExt.Length-1)+"]";
                //Console.WriteLine("==========XXXXXXXXXXX>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<");
                //Console.WriteLine(cajasExt);
                sw.WriteLine(cajasExt);
                sw.Close();
            }
            string sw2;
            var key = SimpleFernet.GenerateKey().UrlSafe64Decode();
            string llaveFinal = Convert.ToBase64String(key);
            using (StreamReader sw = new StreamReader(pathFile))
            {
                sw2  = sw.ReadToEnd();
                var src64 = sw2.ToBase64String();
                var token = SimpleFernet.Encrypt(key, src64.UrlSafe64Decode());
                sw.Close();
                using (StreamWriter sw3 = new StreamWriter(pathFile))
                {
                    sw3.WriteLine(token);
                    sw3.WriteLine(llaveFinal);
                    sw3.Close();
                }
            }
            FileInfo file = new FileInfo(pathFile);
            bool flag = file.Exists;
            if(flag){
                return RedirectToAction("Index");
            }else{
                return Content("Archivo 1 no puede ser encontrado!!!");
            }
            // var resForm = Json(Usuario);
        }
        public IActionResult formRobot2()
        {
            return View();
        }
        [BindProperty]
        public RobotForm2 robotForm2 { get; set; }
        public IActionResult SetformRobot2(){
            Console.WriteLine("===========Data formulario 2=======");
            //var resform1 = Json(robotForm1);
            Console.WriteLine(robotForm2.userNetbank);
            Console.WriteLine(robotForm2.passwordNetbank);
            Console.WriteLine(robotForm2.userSigeco);
            Console.WriteLine(robotForm2.passwordSigeco);
            string fechaFile = DateTime.Now.ToString("yyyy-MM-dd");
            string horaFile = DateTime.Now.ToString("HH") + "H" +DateTime.Now.ToString("mm") + "m" + DateTime.Now.ToString("ss") + "s";
            string pathFile = @"D:\adweb\INPUT\Robot2\" + horaFile +"_"+ fechaFile + "-R2.ini";
            using (StreamWriter sw = new StreamWriter(pathFile))
            {
                sw.WriteLine("[NETBANK]");
                sw.WriteLine("user_netbank = " + robotForm2.userNetbank);
                sw.WriteLine("pass_netbank = " + robotForm2.passwordNetbank);
                sw.WriteLine("[SIGECO]");
                sw.WriteLine("user_sigeco = " + robotForm2.userSigeco);
                sw.WriteLine("pass_sigeco = " + robotForm2.passwordSigeco);
                sw.Close();
            }
            string sw2;
            var key = SimpleFernet.GenerateKey().UrlSafe64Decode();
            string llaveFinal = Convert.ToBase64String(key);
            using (StreamReader sw = new StreamReader(pathFile))
            {
                sw2  = sw.ReadToEnd();
                var src64 = sw2.ToBase64String();
                var token = SimpleFernet.Encrypt(key, src64.UrlSafe64Decode());
                sw.Close();
                using (StreamWriter sw3 = new StreamWriter(pathFile))
                {
                    sw3.WriteLine(token);
                    sw3.WriteLine(llaveFinal);
                    sw3.Close();
                }
            }
            FileInfo file = new FileInfo(pathFile);
            bool flag = file.Exists;
            if(flag){
                return RedirectToAction("Index");
            }else{
                return Content("Archivo 2 no puede ser encontrado!!!");
            }
        }
        //[HttpPost]
        //public IActionResult formRobot2(string a)
        //{
         //   return View();
        //}
    }
}
