﻿using interfazRobot.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;
namespace interfazRobot.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public IActionResult Index()
        {
            ViewBag.saludo = Saludo();
            return View();
        }
        [BindProperty]
        public Usuario Usuario { get; set; }
        public IActionResult SetUsuario(){
            Console.WriteLine("=========funciona el setAction=======");
            var resForm = Json(Usuario);
            return RedirectToAction("Privacy");
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public string Saludo()
        {
            return "Hola mundo desde un metodo simple";
        }
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
