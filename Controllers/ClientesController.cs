﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using interfazRobot.Models;
namespace interfazRobot.Controllers
{
    public class ClientesController : Controller
    {
        public IActionResult Index()
        {
            Cliente c1 = new Cliente() { 
                IdCliente = 1,
                Nombre = "alexander",
                Dui = 1234567,
                Nit = "123213",
                Correo = "a@gmail.com",
            };
            List<Cliente> lc = new List<Cliente>() {
                new Cliente()
                {
                    IdCliente = 2,
                    Nombre = "mayda",
                    Dui = 1234567,
                    Nit = "123213",
                    Correo = "m@gmail.com",
                },
                new Cliente()
                {
                    IdCliente = 3,
                    Nombre = "gabriel",
                    Dui = 1234567,
                    Nit = "123213",
                    Correo = "g@gmail.com",
                }
            };
            lc.Add(c1);
            ViewBag.listaClientes = lc;
            return View();
        }
        public IActionResult Modificar()
        {
            return View();
        }
        public IActionResult Eliminar()
        {
            return View();
        }
    }
}
