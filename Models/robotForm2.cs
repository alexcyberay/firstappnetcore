﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace interfazRobot.Models
{
    public class RobotForm2
    {
        [Required(ErrorMessage = "El campo Usuario de Netbank es requerido.")]
        [MinLength(3, ErrorMessage ="El Usuario Netbank debe tener al menos 3 caracteres.")]
        [MaxLength(5,ErrorMessage ="El Usuario Netbank debe tener no mas de 5 caracteres.")]
        public string userNetbank { get; set; }

        [Required(ErrorMessage = "El campo contraseña de Netbank es requerido.")]
        public string passwordNetbank { get; set; }
        [Required(ErrorMessage = "El campo Usuario de Sigeco es requerido.")]
        [MinLength(4, ErrorMessage ="El Usuario Sigeco debe tener al menos 4 caracteres.")]
        [MaxLength(10,ErrorMessage ="El Usuario Sigeco debe tener no mas de 10 caracteres.")]
        public string userSigeco { get; set; }
        [Required(ErrorMessage = "El campo contraseña de Sigeco es requerido.")]
        public string passwordSigeco { get; set; }
    }
}
