﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace interfazRobot.Models
{
    public class RobotForm1
    {
        [Required(ErrorMessage = "El campo Usuario de Netbank es requerido.")]
        [MinLength(3, ErrorMessage ="El Usuario Netbaank debe tener al menos 3 caracteres.")]
        [MaxLength(5,ErrorMessage ="El Usuario Netbaank debe tener no mas de 5 caracteres.")]
        public string userNetbank { get; set; }
        [Required(ErrorMessage = "El campo contraseña de Netbank es requerido.")]
        public string passwordNetbank { get; set; }
        [Required(ErrorMessage = "El campo Usuario de Mediador es requerido.")]
        [MinLength(4, ErrorMessage ="El Usuario Mediador debe tener al menos 4 caracteres.")]
        [MaxLength(10,ErrorMessage ="El Usuario Mediador debe tener no mas de 10 caracteres.")]
        public string userMediador { get; set; }
        [Required(ErrorMessage = "El campo contraseña de Mediador es requerido.")]
        public string passwordMediador { get; set; }
    }
}
