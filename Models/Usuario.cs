﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace interfazRobot.Models
{
    public class Usuario
    {
        // Validaciones de los campos
        // Atributos de la clase usuario
        [Required(ErrorMessage="Ingrese su nombre.")]
        [MinLength(4, ErrorMessage ="El nombre tiene que tener al menos 4 caracteres.")]
        [MaxLength(20,ErrorMessage ="El nombre debe tener menos de 20 caracteres.")]
        public string Nombres { get; set; }
        [Required(ErrorMessage = "Ingrese su apellido.")]
        [MinLength(4, ErrorMessage = "El apellido tiene que tener al menos 4 caracteres.")]
        [MaxLength(20, ErrorMessage = "El apellido debe tener menos de 20 caracteres.")]
        public string Apellidos { get; set; }
        [Required(ErrorMessage = "Ingrese su Dui.")]
        [RegularExpression("^[0-9]{9}-[0-9]{1}$", ErrorMessage ="Escriba un dui valido")]
        public string Dui { get; set; }
        [Required(ErrorMessage = "Ingrese su correo.")]
        [EmailAddress(ErrorMessage = "Ingrese un correo valido.")]
        public string Correo { get; set; }
        public bool EstadoActivo{ get; set; }
        [Required(ErrorMessage = "Ingrese su fecha de nacimiento.")]
        [DataType(DataType.Date)]
        public DateTime FechaNacimiento { get; set; }
        [Required(ErrorMessage ="Ingrese su edad.")]
        [Range(18,120, ErrorMessage = "El registro es solo para personas mayores de 18 años y menores de 120 años")]
        public int Edad { get; set; }
    }
}
