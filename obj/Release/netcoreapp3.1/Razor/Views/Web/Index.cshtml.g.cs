#pragma checksum "C:\Users\robotltd\source\repos\interfazRobot\Views\Web\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3210b51e00a6993792f6103ecb6356a7c0a86378"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Web_Index), @"mvc.1.0.view", @"/Views/Web/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\robotltd\source\repos\interfazRobot\Views\_ViewImports.cshtml"
using interfazRobot;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\robotltd\source\repos\interfazRobot\Views\_ViewImports.cshtml"
using interfazRobot.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3210b51e00a6993792f6103ecb6356a7c0a86378", @"/Views/Web/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4baa3fc66b6301f8952f16e46f6b6898e8981b35", @"/Views/_ViewImports.cshtml")]
    public class Views_Web_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "web", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "formRobot2", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn_add btn btn-light mb-4"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\robotltd\source\repos\interfazRobot\Views\Web\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<style>
    .widget {
        width: 40%;
        height: 40%;
        margin: auto;
    }

    .widget p {
        display: inline-block;
        line-height: 1em;
    }

    .reloj {
        display: flex;
        font-family: Arial, ""Helvetica Neue"", Helvetica, sans-serif;
        width: 100%;
        padding: 20px;
        font-size: 4em;
        text-align: center;
        background: rgba(0,0,0,.5);
    }
    .section-bg {
        background-color: rgba(42,60,108,.5);
    }
    /*--------------------------------------------------------------
    # Services
    --------------------------------------------------------------*/
    .services .icon-box {
        padding: 15px;
        border-radius: 6px;
        height: 90%;
        background: rgb(248, 97, 9);
    }
    .services .icon-box i {
        /*float: left;*/
        color: rgb(157, 154, 151);
        font-size: 36px;
    }
    .services .icon-box h4 {
        margin-left: auto;
        font-weight: 700;
   ");
            WriteLiteral(@"     margin-bottom: 0px;
        font-size: 14px;
    }
    .services .icon-box h4 a {
        color: rgb(42,60,108);
        transition: 0.3s;
    }
    .services .icon-box .icon-box:hover h4 a {
        color: #ff4a17;
    }
    .services .icon-box p {
        margin-left: 70px;
        line-height: 24px;
        font-size: 14px;
    }
    .services .icon-box:hover h4 a {
        color: rgb(157, 154, 151);
    }
    .section-title {
        padding-bottom: 40px;
        padding-top: 10px;
        opacity: 1;
    }
    .section-title h2 {
        font-size: 14px;
        font-weight: 500;
        padding: 0;
        line-height: 1px;
        margin: 0 0 5px 0;
        letter-spacing: 2px;
        text-transform: uppercase;
        color: #5c8eb0;
        font-family: ""Poppins"", sans-serif;
    }
    .section-title h2::after {
        content: """";
        width: 120px;
        height: 1px;
        display: inline-block;
        background: #ff8664;
        margin: 4px 1");
            WriteLiteral("0px;\r\n    }\r\n    .section-title p {\r\n        margin-top: 10px;\r\n        margin-bottom: 0;\r\n        padding-bottom: 0;\r\n        font-size: 16px;\r\n        font-weight: 700;\r\n        text-transform: uppercase;\r\n        font-family: \"Poppins\", sans-serif;\r\n");
            WriteLiteral("        color: white;\r\n\r\n    }\r\n    .btn_add{\r\n        color: white;\r\n        background: rgba(42,60,108);\r\n        margin-top: 10%;\r\n        margin-bottom: 4px;\r\n        width: 100%;\r\n        font-size: 12px ;\r\n");
            WriteLiteral(@"    }
    .btn_add:hover {
        background: rgb(157, 154, 151);
        color: rgba(42,60,108);
    }
    .btn_add2{
        color: white;
        background: rgb(157, 154, 151);
    }
    .btn_add2:hover {
        background: rgb(248, 97, 9);
    }
    .countdown {
        margin-bottom: 8px;
    }
    .countdown div {
        text-align: center;
        margin: 10px;
        width: 90px;
        padding: 8px 0;
        background: rgba(255, 255, 255, 0.12);
        border-top: 5px solid rgb(248, 97, 9);
    }
    .countdown div h3 {
        font-weight: 700;
        font-size: 26px;
        margin-bottom: 10px;
    }
    .countdown div h4 {
        font-size: 14px;
        font-weight: 600;
    }
    .color-azul{
        color: rgba(42,60,108);
        font-weight: bold;
    }
    .color-plomo{
        color: rgb(157, 154, 151);
        font-weight: bold;
    }
");
            WriteLiteral(@"    .mensajeR1{
        width: 15rem;
        height: 9rem;
        border-radius: 6px;
        background: rgba(42,60,108);
        color: white;
    }
</style>
<div class=""container"">
    <div class=""section-title mb-0 pb-0"">
          <p>Hora Actual 24h
            <i class=""bi bi-clock""></i>
          </p>
    </div>
    <div class=""countdown d-flex justify-content-center mb-0"" data-count=""2023/12/5"">
        <div>
          <h3><p id=""horas"" class=""horas"">00</p></h3>
          <h4 class=""color-azul"">Hors.</h4>
        </div>
        <div>
          <h3><p id=""minutos"" class=""minutos"">00</p></h3>
          <h4 class=""color-azul"">Min.</h4>
        </div>
        <div>
          <h3><p id=""segundos"" class=""segundos"">00</p></h3>
          <h4 class=""color-azul"">Seg.</h4>
        </div>
    </div>
");
            WriteLiteral(@"    <section class=""services section-bg mb-4"">
      <div class=""container"">
        <div class=""section-title mb-0 pb-2"">
            <p>Servicios Digitales SFA's</p>
        </div>
        <p class=""h6"">Seleccione el proceso que desea ejecutar</p>
        <div class=""row"">
            <div class=""col-md-6"" id=""localstorageExist"">
                <!--option message if exist localstorage save -->
            </div>
            <div class=""col-md-6"">
                <div class=""icon-box"">
                    <h4 class=""text-left"">
                        <i class=""bi bi-robot mr-1""></i>
                        <a href=""#"">Proceso 2 - Robot 2</a>
                    </h4>
                    <ol>
                        <li class=""text-left small"">
                            Este servico realiza la tarea sacar reportes del sistema netbank arquedo de caja, saldos, sigeco.
                        </li>
                        <li class=""text-left small"">
                            Llena un ");
            WriteLiteral(@"reporte cheklist lsitando los reportes exsitentes y no exsitentes.
                        </li>
                        <li class=""text-left small"">
                            Recuerde que este proceso debe ejecutarlo una vez realizado el cierre de cajas.
                        </li>
                    </ol>
                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3210b51e00a6993792f6103ecb6356a7c0a8637810156", async() => {
                WriteLiteral("Procesar");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n        </div>\r\n      </div>\r\n    </section>\r\n");
            WriteLiteral(@"</div>
<script>
    function redirectForm1(){
        window.location.href = '/interfazRobot/web/formRobot1';
    }
    window.onload=miDataStorageR1;
    function miDataStorageR1(){
        var Proceso1 = localStorage.getItem('Proceso1');
        console.log(Proceso1);
        if(Proceso1){
            console.log(""siiii"")
            document.getElementById('localstorageExist').innerHTML = ""<div class='icon-box d-flex align-items-center'><div class='mensajeR1 mx-auto text-center'><p class='text-center small mx-2 pt-2'><i class='bi bi-stopwatch'></i>Usted ya proceso su informacion con el Robot1, una vez cerrada sus cajas ejecute el Robot2<a onclick='redirectForm1()' class='btn btn-sm btn_add2 mt-1'>Reprocesar</a></p></div></div>""
        }else{
            document.getElementById('localstorageExist').innerHTML = ""<div class='icon-box'><p onload='miR1();'></p><h4 class='text-left'><i class='bi bi-robot mr-1'></i><a href='#'>Proceso 1 - Robot 1</a></h4><ol class='mb-0'><li class='text-left small'>");
            WriteLiteral(@"Este servico realiza la tarea de extrancion de cajas activas.</li><li class='text-left small'>Extrae pagos de servicios del sistema Netbak y Medidor comparacion para verificar saldos faltantes o irregulares.</li><li class='text-left small'>Recuerde que este proceso debe ejecutarlo antes de realizar el cierre de cajas.</li></ol><a onclick='redirectForm1()' class='btn_add btn btn-light mb-4'>Procesar</a></div>""
        }
    }
    function horaActual() {
        var fecha = new Date();
        hora = fecha.getHours();
        if (hora < 10) {
            hora = ""0"" + hora;
        }
        mireloj = hora;
        return mireloj;
    }
    function minutosActual() {
        var fecha = new Date();
        minutos = fecha.getMinutes();
        if (minutos < 10) {
            minutos = ""0"" + minutos;
        }
        mireloj = minutos
        return mireloj;
    }
    function segundosActual() {
        var fecha = new Date();
        segundos = fecha.getSeconds();
        if (segundos < ");
            WriteLiteral(@"10) {
            segundos = ""0"" + segundos;
        }
        mireloj = segundos
        return mireloj;
    }
    function diaActual() {
        var fecha = new Date();
        var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        diaSemana = fecha.getDay()
        return semana[diaSemana];
    }
    function mesActual() {
        var fecha = new Date();
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        mes = fecha.getMonth()
        return meses[mes];
    }
    function actualizar() {
        //diaSemana = diaActual();
        //mes = mesActual();
        //var fecha = new Date();
        //anio = fecha.getFullYear()
        //dia = fecha.getDate(),
        mihora = horaActual();
        misMinutos = minutosActual();
        misSegundos = segundosActual();
        mirelojHora = document.getElementById(""horas"");
        mirelojHora.in");
            WriteLiteral(@"nerHTML = mihora;
        mirelojMinutos = document.getElementById(""minutos"");
        mirelojMinutos.innerHTML = misMinutos;
        mirelojSegundos = document.getElementById(""segundos"")
        mirelojSegundos.innerHTML = misSegundos;
        //console.log(mihora,"" "",misMinutos,"" "",misSegundos);
        if(mihora == '00' && misMinutos == '00' && misSegundos == '00'){
            expireLocalStorage();
            redirectForm1();
        }
        //document.getElementById(""diaSemana"").innerHTML = diaSemana;
        //document.getElementById(""dia"").innerHTML = dia;
        //document.getElementById(""mes"").innerHTML = mes;
        //document.getElementById(""anio"").innerHTML = anio;
    }
    function expireLocalStorage(){
        localStorage.clear();
    }
    setInterval(actualizar, 1000);
</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
